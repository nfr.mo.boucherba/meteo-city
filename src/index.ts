const title = 'Meteo City !' as string;

document.getElementById('content')!.innerHTML = `${title}`;


/**
 * Convertit une lettre sur 2 en majuscule
 * @param title 
 */
function stringYoYo(title: string): string
{
    const arr = title.split('');

    const newArr = arr.map((letter, index) => {
        if (index % 2 == 0) 
        {
            return letter.toUpperCase();
        }
        
        return letter;
        
    });

    return newArr.join('');
}
//                                                            mon code:
let chercher = document.getElementById("meteo-city");
let bouton = document.getElementById("btn");
let weatherHtml = document.getElementById("weather-html");
let imgWeatherHtml = document.getElementById("img-weather-html");
let villeHtml = document.getElementById("ville-html") ;
let tempHtml = document.getElementById("temp-html");
let tempMaxHtml = document.getElementById("temp-max-html");
let tempMinHtml = document.getElementById("temp-min-html");
let windSpeedHtml = document.getElementById("wind-speed-html");
let humidityHtml = document.getElementById("humidity-html");
let tempAllHtml = document.getElementById("temp-all-html");
let directionWindHtml = document.getElementById("direction-wind-html");
//@ts-ignore
imgWeatherHtml.src = `../images/weather-img.png`

bouton.addEventListener('click', ()=> {
    //@ts-ignore
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${chercher.value}&units=metric&appid=4cd279414f5bb17565329f9f07f10b18`)
    .then(resp => resp.json())

    .then(data => {

        
        villeHtml.innerHTML = `Le nom de la ville: ${data.name}`
        //@ts-ignore
        imgWeatherHtml.src = `https://openweathermap.org/img/w/${data.weather[0].icon}.png`;
        weatherHtml.innerHTML = `Le météo aujourd'hui: ${data.weather[0].main}`;
        tempHtml.innerHTML = `La temperature ressenties: ${data.main.temp} °C`;
        tempMaxHtml.innerHTML = `La temperature maximale: ${data.main.temp_max} °C`;
        tempMinHtml.innerHTML = `La temperature minimale: ${data.main.temp_min} °C`;
        windSpeedHtml.innerHTML = `La vitesse et du vent: ${data.wind.speed} km/h`;
        humidityHtml.innerHTML = `Le taux d'humidité: ${data.main.humidity} %`;
        tempAllHtml.innerHTML = `les temperature:`;
        directionWindHtml.innerHTML = `La direction du vent: ${data.wind.deg} °`;

    })
    .catch(error => {
        console.log(error);
        
    })
})
