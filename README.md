# Meteo-City

Meteo-City

# Ressource(s)

## fetch

https://www.digitalocean.com/community/tutorials/how-to-use-the-javascript-fetch-api-to-get-data-fr

## geo api

https://geo.api.gouv.fr/

## openweathermap

https://openweathermap.org/price

## Conventional Commits

https://www.conventionalcommits.org/en/v1.0.0/


# Contexte du projet
Vous etes chargé de developper une page web responsive permettant de consulter la meteo du jour.

Le site ne contiendra qu'une seule page contenant :

un input permettant de rentrer le nom d'une ville, ainsi qu'un bouton pour lancer la recherche
le reste de la page est consacrée à l'affichage de la meteo, qui devra contenir
le nom de la ville
une image representative (ensoleillé, nuageux, etc)
les temperature, minimale, maximale, ressenties
la vitesse et direction du vent
le taux d'humidité
L'api meteo se trouve ici https://openweathermap.org/price (Creer un compte gratuit biensur)

Creer un wireframe avant de commencer a coder

Afin de permettre une évolution ultérieure, le site devra être développé avec le framework css Bootstrap 5.

Bonus: auto complétion des villes dans l'input de recherche avec l'api https://geo.api.gouv.fr/

Bonus: sur mobile, la recherche peut se faire via la position gps

# Modalités pédagogiques
Travail individuel

Commit réguliers en respectant la spécification de Conventional Commits

# Critères de performance
Le site correspond au wireframe
La recherche de météo fonctionne
L'image est représentative
Le site est développé avec Bootstrap 5
Le site est adaptatif (responsive desktop / mobile)
Le site est en ligne
Les commits sont réguliers et explicites

# Livrables
- Dépôt gitlab
- Site en ligne

# Template Webpack Typescript

## Requirements

[Node.js](https://nodejs.org) is required to install dependencies and run scripts via `npm`.

## Writing Code

Download the repo, **DON'T CLONE** it, run `npm install` from your project directory. Then, you can start the local development
server by running `npm start`.

After starting the development server with `npm start`, you can edit any files in the `src` folder
and webpack will automatically recompile and reload your server (available at `http://localhost:8080`
by default).

## Available Commands

| Command | Description |
|---------|-------------|
| `npm install` | Install project dependencies |
| `npm start` | Build project and open web server running project |
| `npm run build` | Builds code bundle with production settings (minification, uglification, etc..) |